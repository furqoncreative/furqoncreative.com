window.addEventListener("resize", toggleLogo);
window.addEventListener("load", toggleLogo);

let mobile = window.matchMedia("(max-width: 735px)");
let dekstop = window.matchMedia("(min-width: 735px)");

function toggleLogo(){
    if (mobile.matches) {
        document.getElementById("logo-mobile").classList.remove("hidden");
        document.getElementById("logo-desktop").classList.add("hidden");
    }
    
    if (dekstop.matches) {
        document.getElementById("logo-mobile").classList.add("hidden");
        document.getElementById("logo-desktop").classList.remove("hidden");
    }
}