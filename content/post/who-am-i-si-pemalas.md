+++
authors = ["Deden Muhamad Furqon"]
date = 2019-01-13T17:00:00Z
draft = true
excerpt = " Akhirnya, setelah sekian lama tidak update di blog ini i'm back. Jika ada yang tanya kenapa baru update setelah sekian lama, maka jawabannya tepat ada pada judul post ini."
hero = "/images/15.jpg"
title = "Who Am I ? : Si Pemalas."

+++
[Sumber Gambar : thelogicalindian.com](https://draft.blogger.com/u/2/blog/post/edit/4928447261853594161/1197466852783330084#)

Baca Juga :

* [Who Am I ? : Pembukaan](https://draft.blogger.com/u/2/blog/post/edit/4928447261853594161/1197466852783330084#)

Akhirnya, setelah sekian lama tidak update di blog ini _i'm back_. Jika ada yang tanya kenapa baru update setelah sekian lama, maka jawabannya tepat ada pada judul _post_ ini.

Ya, jujur harus saya katakan bahwa saya adalah seorang pemalas, dan jujur juga saya tidak merasa bangga dan tidak merasa nyaman dalam kondisi seperti ini. Mungkin yang sudah membaca [Who Am I ? : Pembukaan ](https://draft.blogger.com/u/2/blog/post/edit/4928447261853594161/1197466852783330084#)pasti bertanya "loh kenapa di situ anda bilang seorang yang ambisius dan teguh pada pendirian ?". Memang pada dasarnya saya seorang yang ambisius, tapi perlu diingat juga kalau di [Who Am I ? : Pembukaan](https://draft.blogger.com/u/2/blog/post/edit/4928447261853594161/1197466852783330084#) juga saya sudah bilang bahwa saya adalah orang ''aneh". Saya adalah ambisius ketika sedang mengikuti sesuatu seperti kompetisi, saya adalah ambisius ketika sedang mengerjakan sesuatu yang memiliki target atau batas waktu, dan saya adalah ambisius ketika memiliki tanggungjawab kepada orang lain. Di luar itu semua sebagian besar saya adalah pemalas, saya pemalas ketika libur panjang, saya pemalas ketika berkarya yang hanya diperintahkan oleh "si Ambisius" tanpa ada tanggung jawab kepada orang lain, saya adalah pemalas ketika mengerjakan jadwal rutinitas yang dibuat oleh "si Ambisius". Memang tidak banyak orang mengetahui hal ini, bahkan teman dekat sekalipun, kecuali yang benar-benar dekat. Karena ketika di luar sana, ketika dihadapan orang lain saya selalu berusaha menjadi "si ambisius" dan tidak ingin menampilkan kemalasan saya. Bukan, bukan maksud saya ingin terlihat sempurna, bukan jaga image, bukan cari perhatian, bukan juga saya munafik. Hanya saja ketika di luar, ketika sedang berada disekitar orang, selalu ada motivasi atau dorongan untuk menjadi "si ambisius", tidak peduli orang-orang memperhatikan saya atau tidak, melihat saya atau tidak, bahkan orang yang saya kenal atau bukan, saya tetap akan menjadi "si ambisius" di sekitar orang tersebut. Dan satu hal lagi, meski saya seolah ingin terlihat sempurna di hadapan orang, tapi saya bukanlah orang yang suka mencari perhatian atau ingin dipuji orang lain, bukan, saya bukan orang seperti itu.

Intinya adalah, saya senang menjadi "si ambisius" tapi bukan untuk mendapat perhatian orang lain, bukan juga untuk dipuji, saya senang menjadi "si ambisius" karena menurut saya seperti itulah seharusnya saya. Orang-orang diluar sana saya jadikan sebagai pengawas bagi diri saya sendiri agar selalu menjadi "si ambisius". Tapi, ketika di rumah, ketika sendirian, ketika tidak memiliki tugas, ketika tidak memiliki tanggungjawab kepada orang lain, "si pemalas"-lah yang menguasai diri. Malas disini adalah malas dari melakukan hal-hal yang produktif, malas dari belajar, malas dari berkarya dan malas dari hal produktif lainnya. Memang terkadang rasa malas itu diperlukan untuk sedikit bersantai dan menghilangkan penat dari segala rutinitas. Tapi malas yang saya alami ini adalah malas yang menguasi diri, bahkan saya sadar ketika malas tapi jika tidak ada motivasi yang kuat, saya tidak bisa melawan malas tersebut. Tentu saya tidak menyerah begitu saja, saya tidak ingin "si pemalas" ini terus menerus menguasai diri saya. Menulis hal ini, menceritakan kepribadian saya, membahas topik tentang malas adalah salah satu usaha saya untuk mengusir "si pemalas" dari diri saya. Siapa tau setelah ada yang membaca tulisan ini kemudian orang itu menghubungi saya untuk memberi motivasi atau meberikan tips untuk menghilangkan rasa malas atau bahkan hanya untuk sekedar berbagi cerita tentang kemalasan.

Kemalasan saya ini terjadi juga pada pengembangan dan penulisan blog ini, dimana _postingan_ terakhir blog ini adalah lebih dari satu tahun yang lalu,

[![](https://4.bp.blogspot.com/-7iJfYRQQOPw/XDyraM7MR2I/AAAAAAAAALY/SuAdshaxcAc7I1expVjq6CDGzVUqvOYEwCLcBGAs/s400/2.PNG =400x91)](https://draft.blogger.com/u/2/blog/post/edit/4928447261853594161/1197466852783330084#)

Postingan terakhir blog

bahkan tulisan ini pun, sudah saya mulai dari bulan agustus 2018, ya saya masih ingat saat itu sedang libur semester 2. Ketika sedang malas-malasnya, saya mecoba untuk menulis tapi tidak selesai-selesai padahal cuma sedikit. Dan tingkat kemalasan yang paling parah adalah ketika Semester 3 kemarin, padahal kegiatan kuliah tidak banyak namun tidak ada satu karya pun yang saya keluarkan/terbitkan, baik itu cerita, puisi, blog, design, dan yang lainnya. Bahkan untuk posting di Instagram pun tidak pernah. _Damn_, betapa malasnya.

[![](https://4.bp.blogspot.com/-NIgRqYubi68/XDyuZ16RDCI/AAAAAAAAALs/GtypeAXyHZ05OtvRsdYMc6YjPH25t3LiQCLcBGAs/s400/3.PNG =400x116)](https://draft.blogger.com/u/2/blog/post/edit/4928447261853594161/1197466852783330084#)

Postingan terakhir Instagram

Ya, harus diaku semester 3 kemarin adalah semester paling tidak produktif sampai saat ini, dan semoga itu tidak terulang lagi pada semester berikutnya..

Oke mungkin cukup sekian untuk hari ini, karena sepertinya "si pemalas" mulai merayu untuk segera berhenti menulis. bye.