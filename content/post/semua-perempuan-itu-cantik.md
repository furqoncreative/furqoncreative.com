+++
authors = ["Deden Muhamad Furqon"]
date = 2019-01-14T17:00:00Z
draft = true
excerpt = "Sebelum membaca tulisan ini lebih jauh, sebaiknya persiapkan diri anda sebaik mungkin, karena tulisan ini mungkin akan sedikit panjang dan serius. Maka dari itu anda bisa memulainya dengan mengambil posisi duduk yang nyaman, menyiapkan beberapa cemilan, dan sebagai pelengkap anda bisa memilih teh atau kopi sesuai selera anda."
hero = "/images/semua-perempuan-itu-cantik.png"
tags = ["opinion"]
title = "Semua Perempuan itu Cantik"

+++
##### **Pendahuluan**

Tulisan ini pertama kali saya tulis pada tahun 2019. Saat itu, entah kenapa ide tentang tulisan ini muncul begitu saja. Sekarang, di tahun 2022, saya sadar apa alasannya. 

Pada dasarnya saya itu gampang suka sama seseorang, perempuan tentunya. Rasa suka ini bisa disebabkan oleh banyak hal, bahkan sesederhana saling tatap sebentar atau dipertemukan tidak sengaja dalam waktu dan tempat yang berasamaan. Dan rasa suka tersebut bisa datang tiba-tiba, mungkin anak zaman sekarang menyebutnya dengan kata baper. Hal tersebut bukan terjadi bukan sekali atau dua kali terjadi. Sampai kemudian, satu hal yang saya sadari bahwa perempuan yang saya sukai tersebut memiliki bentuk wajah yang berbeda dan bukan dalam tipikal yang mirip. Mungkin terdengar naif atau munafik bagi sebagian orang, meskipun mungkin ada yang mengatakannya tidak cantik pada mereka, tetapi pada akhirnya saya menyadari setiap perempuan tersebut mempunyai kecantikannya masing-masing. Kenapa saya bisa berfikir demikian? **_this is why_**.

##### **Tidak Ada Yang Tidak Cantik**

Tidak ada yang tidak cantik, sekali lagi saya tegaskan dari subjudul diatas. Di sini saya tidak menggunakan kata jelek untuk anonim dari kata cantik. Kenapa? karena pertama, memang tidak ada yang jelek (atau lebih tepatnya tidak cantik. Kedua, karena menurut saya kita tidak selayaknya mengatakan jelek kepada ciptaan tuhan yang maha sempurna, kita semua tahu tuhan telah menciptakan manusia dengan sedemikian rupa dan sesempurna mungkin baik itu laki-laki maupun perempuan. Dan saya menulis seperti ini bukan berarti saya tidak pernah mengatakan jelek kepada manusia, dengan menulis seperti ini sebenarnya bukan hanya untuk berbagi kepada anda semua melainkan agar menjadi _self reminder_ bagi diri saya sendiri. Mengenai kata jelek itu sendiri lebih cocok digunakan untuk barang atau hal apapun buatan manusia yang pada dasarya tidak ada yang sempurna. Dan sekarang saya ingin agar kita bersepakat untuk tidak menggunakan kata jelek untuk manusia.

##### **Tidak Ada Tingkatan Kecantikan**

Menyinggung dari subjudul pertama, memang tidak ada perempuan yang tidak cantik (karena kita telah sepakat untuk tidak menggunakan kata jelek) di dunia ini. Karena kata tidak cantik yang sering kita gunakan selama ini adalah kata ganti untuk membandingkan perbedaan kecantikan seseorang dengan kecantikan yang telah menjadi _stereotype_ di masyarakat atau di seluruh dunia dan di pikiran kita. Disini saya menggunakan **perbedaan kecantikan** bukan **tingkatan kecantikan**. Karena ketika kita menggunakan kata tingkatan, maka selalu ada kata kurang dan lebih, rendah dan tinggi, bawah dan atas, yang jika dikaitkan dengan kecantikan maka pasti ada kata **kurang cantik** dan **lebih cantik.** Maka dari itu saya menggunakan kata perbedaan kecantikan, karena memang itu adanya. Dengan kata perbedaan tidak ada lagi kata lebih dan kurang, yang ada hanyalah keragaman, variasi, dan keunikan tersendiri dari sebuah kecantikan.

##### **Tidak Ada Standar Kecantikan**

Setelah kita “menghilangkan” kata tidak cantik dan tingkatan kecantikan, maka selanjutnya kita harus “menghilangkan” standar kecantikan. Mengapa? Karena memang sebenarnya tidak ada yang dinamakan standar kecantikan, yang ada hanyalah _stereotype_ yang dibangun dimasyarakat tentang definisi kecantikan. Misalnya, yang cantik itu yang bertubuh langsing, yang berhidung mancung, yang berkulit putih, yang berwajah tirus, dan _stereotype-stereotype_ lainnya. Itu Semua bukanlah standar kecantikan, itu semua adalah salah satu dari variasi dan keragaman kecantikan. Dan kalaupun itu semua merupakan standar kecantikan, saya cukup yakin tidak semua orang setuju dengan standar kecantikan tersebut, karena setiap orang khususnya laki-laki pasti memiliki "standar" kecantikannya masing-masing. Jadi, tidak ada yang namanya standar khusus dalam kecantikan, semua perempuan cantik dengan karakternya masing-masing, semua perempuan cantik dengan variasinya masing-masing, dan semua perempuan cantik dengan kecantikannya masing-masing.

#### **Semua Perempuan Memang Cantik**

Oke, setelah menghilangkan tiga kata “negatif” yang telah dibahas pada tiga subjudul diatas, sedikitnya saya harap kita sama-sama mulai menyadari kalau semua perempuan itu memang cantik. Yang sekarang perlu kita ketahui adalah perbedaan cara melihat atau menyadari kecantikan seseorang. Karena bagaimanapun, kita tidak mungkin bisa lepas 100% dari _stereotype_ kecantikan yang telah ada di memory otak kita. Maka dari itu, perbedaan cara melihat atau menyadari kecantikan seseorang itu dari cepat atau lamanya kita menyadari. Semakin dekat ciri-cirinya dengan _stereotype_ dalam memory kita maka akan semakin cepat kita menyadarinya bahwa perempuan tersebut memang cantik. Namun, apabila jauh dari _stereotype_ dalam memory kita, maka kita hanya perlu waktu untuk menyadari kencantikannya. “Lah kalau gitu mah sama aja ada yang cantik ada yang engga ?”. Eitss tunggu dulu, perlu diingat bahwa kita memiliki _stereotype_ yang berbeda-beda dalam memory tentang kecantikan. Jadi tidak semua perempuan itu disadari kecantkannya dengan cepat oleh setiap laki-laki, dan begitupun sebaliknya. **Pasti** setiap perempuan selalu ada laki-laki yang dengan cepat menyadari kecantikannya, yang artinya, mari sama-sama kita tarik kesimpulan bahwa **Semua Perempuan Memang Cantik**.

##### **Penutup**

Ah, memang sedikit rumit tulisan saya ini, mudah-mudah anda yang membaca dapat memahaminya. Baik, sepertinya cukup sekian untuk kesempatan kali ini. Mohon maaf apabila kesalahan, dan jika ada pendapat lain boleh langsung hubungi saya. Terima kasih.